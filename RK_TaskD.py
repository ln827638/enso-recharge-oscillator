"""
Student Id: ln827638
MTMW14 Ocean Recharge Oscillator

Task D
"""

import numpy as np
import matplotlib.pyplot as plt
# Read in all the parameters and functions associated with this application
from Parameters_Functions import *


# Setting up the number of cycles and time steps
tmin = 0
tmax = 5*41  #months (41 months = 1 cycle)
Nt = 500
dt = ((tmax - tmin)/Nt)/30   # 0.41 months  = 12.3 days
dt_nondim = dt/2
nt = Nt*30

t = np.zeros(nt)
t[0] = 0
t[1] = t[0] + dt_nondim

#Initialising SST anomaly and ocean thermocline depth
Tinitial = 1.125/7.5 
T = np.zeros(nt)
T[0] = Tinitial

hinitial = 0
h = np.zeros(nt)
h[0] = hinitial

# Array to store values of mu
mu_array = np.zeros(nt)


def RungeKutta_D(T, h, t, nt):
     """Running the coupled ocean recharge oscillator model using Runge-Kutta 
     time scheme, using the given parameters for Task D,for time step dt for
     nt time steps"""

     for it in range(0, nt-1):
         
         mu_var = mu_o*(1 + mu_annual*np.cos(2*np.pi*t[it]/tau_nondim - \
                                             5*np.pi/6))
         b = bo*mu_var
         R = gamma*b - c


         K1 = SST(T[it],h[it],gamma, epsilon, R,b, chi)
         L1 = Depth(T[it], h[it], r, alpha, b, chi)

         K2 = SST(T[it] + K1*dt_nondim/2,h[it] + L1*dt_nondim/2,gamma,\
                  epsilon, R, b, chi)
         L2 = Depth(T[it]+ K1*dt_nondim/2, h[it]+ L1*dt_nondim/2, r, alpha, \
                    b, chi)

         K3 = SST(T[it] + K2*dt_nondim/2,h[it] + L2*dt_nondim/2,gamma, \
                  epsilon, R, b, chi)
         L3 = Depth(T[it]+ K2*dt_nondim/2, h[it]+ L2*dt_nondim/2, r, alpha, \
                    b, chi)

         K4 = SST(T[it] + K3*dt_nondim,h[it] + L3*dt_nondim,gamma, epsilon, \
                  R, b, chi)
         L4 = Depth(T[it]+ K3*dt_nondim, h[it]+ L3*dt_nondim, r, alpha, \
                    b, chi)

         T[it+1] = T[it] + 1/6*dt_nondim*(K1 + 2*K2 + 2*K3 + K4)
         h[it+1] = h[it] + 1/6*dt_nondim*(L1 + 2*L2 + 2*L3 + L4)
         
         t[it+1] = t[it] + dt_nondim
         mu_array[it] = mu_var

     return T, h, t, mu_array
 
RK_TaskD = RungeKutta_D(T, h, t, nt)

# Re-dimensionalise T, h and t using their scale values- 7.5K, 150m and 
# 2 months respectively
RK_T_D = RK_TaskD[0]*7.5
RK_h_D = RK_TaskD[1]*150
RK_t_D = RK_TaskD[2]*2
RK_mu_D = RK_TaskD[3]