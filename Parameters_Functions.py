"""
Student Id: ln827638
MTMW14 Ocean Recharge Oscillator

Model Constants and Functions for prognostic variables (T, and h)
"""
#Code to present all the parameters requred for the coupled equations of 
# SST anomaly (T) and ocean thermocline depth (h) and defining functions 
# for equations of T and h
import numpy as np

# Parameters
gamma = 0.75
bo = 2.5
c = 1
r = 0.25
alpha = 0.125
epsilon = 0.1   # nonlinearity
mu = 2/3
b = bo*mu
R = gamma*b - c
chi = 0     # wind stress forcing

# For when considering annual cycle of mu in Tasks D,E and F
mu_o = 0.75
mu_annual = 0.2
tau = 12        # months
tau_nondim = tau/2

# For when considering annual cycle of chi in Tasks E and F
f_annual = 0.02
f_random = 0.2
tau_cor = 1/30 #day


# Function for equation east Pacific Sea Surface Temperature anomaly (T)
def SST(T, h, gamma, epsilon, R, b, chi):
   """ To define the equation for east Pacific Sea Surface Temperature
   anomaly, T"""

   return R*T + gamma*h - epsilon*(h + b*T)**3 + gamma*chi


# Function for west Pacific ocean thermocline depth (h)
def Depth(T, h, r, alpha, b, chi):
   """ To define the equation for west Pacific ocean thermocline 
   depth (h)"""

   return -r*h - alpha*b*T - alpha*chi

