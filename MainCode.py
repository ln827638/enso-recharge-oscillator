"""
Student Id: ln827638
MTMW14 Ocean Recharge Oscillator

Main Code
"""
# Outer code for setting up the ocean recharge oscillator model
# and calling the functions to run the model and plot the results of all Tasks
import numpy as np
import matplotlib.pyplot as plt

# Read in all the parameters, functions and other code for all the tasks
# associated with this application
from Parameters_Functions import *
from RK_TaskA import *
from RK_TaskD import *
from RK_TaskE import *
from RK_TaskF import *


# Setting up the number of cycles and time steps
tmin = 0
tmax = 5*41  #months (41 months = 1 cycle)
Nt = 500
dt = ((tmax - tmin)/Nt)/30   # 0.41 months  = 12.3 days
dt_nondim = dt/2
nt = Nt*30

# Time axis points for plotting
taxis = np.linspace(tmin, tmax, nt)

#Plot the solutions from all Tasks

# Results will be shown for particular tasks when their associated conditions 
# mentioned in the project are applied in the model
print('Results for Task A/B/C')
font = {'size'   : 15}
plt.rc('font', **font)
plt.plot(taxis, RK_T_A,color = 'red', label = 'Te: SST (K)') 
plt.plot(taxis, RK_h_A,color = 'blue', label = 'hw: Thermocline depth (m)')
plt.ylabel('Te,hw')
plt.xlabel('Time (months)')
plt.legend(bbox_to_anchor=(1.1, 1))
plt.show()

plt.clf()
plt.plot(RK_T_A, RK_h_A)
plt.xlabel('Te')
plt.ylabel('hw')
plt.show()

plt.clf()
print('\n')
print('Results for Task D')
plt.plot(RK_t_D, RK_T_D,color = 'red', label = 'Te: SST (K)') 
plt.plot(RK_t_D, RK_h_D,color = 'blue', label = 'hw: Thermocline depth (m)')
plt.ylabel('Te,hw')
plt.xlabel('Time (months)')
plt.legend(bbox_to_anchor=(1.1, 1))
plt.show()

plt.clf()
plt.plot(RK_T_D, RK_h_D)
plt.xlabel('Te')
plt.ylabel('hw')
plt.show()


plt.clf()
print('\n')
print('Results for Task E')
plt.plot(RK_t_E, RK_T_E,color = 'red', label = 'Te: SST (K)') 
plt.plot(RK_t_E, RK_h_E,color = 'blue', label = 'hw: Thermocline depth (m)')
plt.ylabel('Te,hw')
plt.xlabel('Time (months)')
plt.legend(bbox_to_anchor=(1.1, 1))
plt.show()

plt.clf()
plt.plot(RK_T_E, RK_h_E)
plt.xlabel('Te')
plt.ylabel('hw')
plt.show()

plt.clf()
print('\n')
print('Results for Task F')
print('\n')

print('Perturbing initial T')
for temp in T_all:
  plt.plot(RK_t_E,temp)
  plt.ylabel('Te (K)')
  plt.xlabel('Time (months)')

plt.show()
plt.clf()

print('Perturbing initial h')
for height in h_all:
  plt.plot(RK_t_E, height)
  plt.ylabel('hw (m)')
  plt.xlabel('Time (months)')

plt.show()




