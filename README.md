# ENSO Recharge Oscillator

Run MainCode.py to run the model and plot results of all tasks of the project

Parameters_Functions.py contains all constants and parameters required in the 
model and the functions of SST anomaly and thermocline depth

For Task A, set the epsilon in Parameters_Functions.py as 0 and then run the 
main code to obtain results for task A

For other tasks, change the parameters as mentioned in  Parameters_Functions.py
and then run the main code which will then show results of those tasks

In Task F, ensemble size can be changed in RK_TaskF.py file, also remember to 
change the peturbations size in T and h so it is same as the ensemble size.
Run the main code to show the results for Task F

