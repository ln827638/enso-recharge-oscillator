"""
Student Id: ln827638
MTMW14 Ocean Recharge Oscillator

Task F
"""
# Read in all the parameters and functions associated with this application
from RK_TaskE import *


# White noise for peturbations in Temperature and thermocline depth
Tpert = np.random.uniform(-0.5,0.5,10)
hpert = np.random.uniform(-1,1,10)

# Ensemble

T_all = []

h_all = []

ensemble_size = 10

# Size in perturbations and ensemble size should be the same. 
# Change the size when ensemble size is changed as well

for member in range(ensemble_size):
    Tinitial = (1.125 + Tpert[member])/7.5
    T[0] = Tinitial
    
    hinitial = (0 + hpert[member])/150 
    h[0] = hinitial
    RK_TaskF = RungeKutta_E(T, h, t, nt)
    
    T_all.append(RK_TaskF[0])

    h_all.append(RK_TaskF[1])

