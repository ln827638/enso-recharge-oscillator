"""
Student Id: ln827638
MTMW14 Ocean Recharge Oscillator

Task A
"""


import numpy as np
import matplotlib.pyplot as plt
# Read in all the parameters and functions associated with this application
from Parameters_Functions import *


# Setting up the number of cycles and time steps
tmin = 0
tmax = 5*41  #months (41 months = 1 cycle)
Nt = 500
dt = ((tmax - tmin)/Nt)/30   # 0.41 months  = 12.3 days
dt_nondim = dt/2
nt = Nt*30

#Initialising SST anomaly and ocean thermocline depth
Tinitial = 1.125/7.5 
T = np.zeros(nt)
T[0] = Tinitial

hinitial = 0
h = np.zeros(nt)
h[0] = hinitial


def RungeKutta(T, h, nt):
     """Running the coupled ocean recharge oscillator model using Runge-Kutta 
     time scheme, using the given parameters,for time step dt for nt time
     steps"""

     for it in range(0, nt-1):
         
         K1 = SST(T[it],h[it],gamma, epsilon, R,b, chi)
         L1 = Depth(T[it], h[it], r, alpha, b, chi)
         
         K2 = SST(T[it] + K1*dt_nondim/2,h[it] + L1*dt_nondim/2,gamma, epsilon, \
                  R, b, chi)
         L2 = Depth(T[it]+ K1*dt_nondim/2, h[it]+ L1*dt_nondim/2, r, alpha, \
                    b, chi)
         
         K3 = SST(T[it] + K2*dt_nondim/2,h[it] + L2*dt_nondim/2,gamma, epsilon, \
                  R, b, chi)
         L3 = Depth(T[it]+ K2*dt_nondim/2, h[it]+ L2*dt_nondim/2, r, alpha, \
                    b, chi)
         
         K4 = SST(T[it] + K3*dt_nondim,h[it] + L3*dt_nondim,gamma, epsilon, \
                  R, b, chi)
         L4 = Depth(T[it]+ K3*dt_nondim, h[it]+ L3*dt_nondim, r, alpha, \
                    b, chi)

         T[it+1] = T[it] + 1/6*dt_nondim*(K1 + 2*K2 + 2*K3 + K4)
         h[it+1] = h[it] + 1/6*dt_nondim*(L1 + 2*L2 + 2*L3 + L4)
         
        
     return T, h
 
RK_TaskA = RungeKutta(T, h, nt)

# Re-dimensionalise T, and h using their scale values- 7.5K and 150m 
# respectively
RK_T_A = RK_TaskA[0]*7.5
RK_h_A = RK_TaskA[1]*150